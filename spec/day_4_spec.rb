require_relative "../ruby/day_4.rb"

RSpec.describe FourthDay do
  let(:inputs) do
    input_file = File.join(File.dirname(__FILE__), '../inputs/day-4.txt')
    File.read(input_file)
  end

  describe ".part_1" do
    subject { described_class.part_1(inputs) }
    it do
      expect(subject).to be_truthy
    end
  end

  describe ".part_2" do
    subject { described_class.part_2(inputs) }

    it do
      expect(subject).to be_truthy
    end
  end
end

RSpec.describe FourthDay::Board do
  subject { described_class.new("1 2 3\n4 5 6\n7 8 9") }

  it do
    subject.draw(9)
    expect(subject.matches).to include([2, 2] => "9")
  end

  it do
    subject.draw(1)
    expect(subject).to_not be_winner
    subject.draw(2)
    expect(subject).to_not be_winner
    subject.draw(3)
    expect(subject).to be_winner
  end

  it do
    subject.draw(1)
    expect(subject).to_not be_winner
    subject.draw(5)
    expect(subject).to_not be_winner
    subject.draw(9)
    expect(subject).to be_winner
  end

  it do
    subject.draw(3)
    expect(subject).to_not be_winner
    subject.draw(5)
    expect(subject).to_not be_winner
    subject.draw(7)
    expect(subject).to be_winner
  end
end
