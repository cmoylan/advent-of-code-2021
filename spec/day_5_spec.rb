require_relative "../ruby/day_5.rb"

RSpec.describe FifthDay, :current do
  let(:inputs) do
    input_file = File.join(File.dirname(__FILE__), '../inputs/day-5.txt')
    File.read(input_file)
  end

  describe "part 1" do
    subject { described_class.new(inputs).part_1 }

    it { subject }
  end

  describe "part 2" do
    subject { described_class.new(inputs).part_2 }

    it { subject }
  end
end

RSpec.describe FifthDay::Vent, :xcurrent do
  subject { described_class.new(*inputs) }

  context "horizontal" do
    let(:inputs) { [[1,5], [1,20]]}

    it { is_expected.to be_horizontal }
  end

  context "vertical" do
    let(:inputs) { [[7,5], [11,5]]}

    it { is_expected.to be_vertical }
  end

  context "points - horizontal" do
    let(:inputs) { [[1,5], [1,7]]}

    it do
      expect(subject.points).to eq( [[1,5], [1,6], [1,7]] )
    end
  end

  context "points - vertical" do
    let(:inputs) { [[3,5], [1,5]]}

    it do
      expect(subject.points).to eq( [[1,5], [2,5], [3,5]] )
    end
  end
end
