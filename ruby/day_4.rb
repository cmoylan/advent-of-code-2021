#!/usr/bin/env ruby

class FourthDay
  class Board
    attr_reader :squares, :matches

    def initialize(initial)
      @squares = initial.split("\n").map{ |row| row.split(" ") }
      # [y, x] => number
      @matches = {}
    end

    def to_s
      @squares.map{ |row| row.join(" ") }.join("\n")
    end

    def draw(number)
      return if winner?

      number = number.to_s

      if result = @squares.flatten.find_index(number)
        width = @squares.first.count
        y = result / width
        x = result % width
        @matches[[y, x]] = number
      end
    end

    def winner?
      width = @squares.first.count

      # down
      numbers = @matches.keys.map(&:first)
      y_counts = numbers.uniq.map{ |n| [n, numbers.count(n)] }
      return true if y_counts.any?{ |c| c.last >= width }

      # across
      numbers = @matches.keys.map(&:last)
      x_counts = numbers.uniq.map{ |n| [n, numbers.count(n)] }
      return true if x_counts.any?{ |c| c.last >= width }

      # diagonal [0,0] to [4,4]
      return true if (0..width-1).all?{ |i| @matches.has_key?([i, i]) }

      # diagonal [4,0] [3,1] [2,2] [1,3] [0,4]
      return true if (0..width-1).all?{ |i| @matches.has_key?([width-1 - i, i]) }

      false
    end

    def score
      # sum of all unmarked numbers
      total = @squares.flatten.map(&:to_i).sum
      unmarked = total - @matches.values.map(&:to_i).sum

      unmarked * @matches.values.last.to_i
    end
  end

  def self.part_1(inputs)
    inputs = inputs.split("\n\n")
    moves = inputs.shift.split(",")
    boards = inputs.map{ |b| Board.new(b) }

    moves.each do |move|
      boards.each{ |b| b.draw(move) }

      if winner = boards.find(&:winner?)
        puts "\nfirst winner:"
        puts winner.score
      end
      print "#{move},"
    end
    puts ""
  end

  def self.part_2(inputs)
    inputs = inputs.split("\n\n")
    moves = inputs.shift.split(",")
    boards = inputs.map{ |b| Board.new(b) }

    last_winner = nil
    puts "checking reverse"
    moves.each do |move|
      boards.each{ |b| b.draw(move) }

      if winner = boards.find(&:winner?)
        last_winner = boards.delete(winner)
        if boards.count == 1
          binding.pry
        end
      end
      print "#{move},"
    end

    # FIXME not quite working
    binding.pry
    puts last_winner

    puts ""
  end
end
