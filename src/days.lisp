(in-package :cl-user)
(defpackage aoc2021/days
  (:use :cl :iterate)
  (:import-from :tools
                :parse-file-into-list
                :parse-file-into-number-list
                :parse-file-into-list-by-blank-line
                :abspath
                :empty-string-p
                :split-str)
  (:import-from :defclass-std :defclass/std)
  (:export :day-1 :day-2 :day-3 :day-4))
(in-package :aoc2021/days)


(defun day-1 ()
  "the first day"
  (defun part-1 ()
    "Count how many times subsequent numbers increase"
    (let* ((numbers-list (parse-file-into-number-list (abspath "inputs/day-1.txt")))
           (count 0)
           (previous (first numbers-list)))

      (iterate (for num in numbers-list)
               (if (> num previous)
                   (setf count (+ 1 count)))
               (setf previous num))
      count))

  (defun part-2 ()
    "Count how many times 3 subsequent numbers increase"
    (let* ((numbers-list (parse-file-into-number-list (abspath "inputs/day-1.txt")))
           (count 0)
           (x (first numbers-list))
           (y (second numbers-list))
           (previous-sum (reduce '+ (subseq numbers-list 0 3))))

      (iterate (for num in (subseq numbers-list 2))
               (print (list x y num))
               (if (> (reduce '+ (list x y num)) previous-sum)
                   (progn
                     (setf count (+ 1 count))))
               (setf previous-sum (reduce '+ (list x y num)) )
               (setf x y)
               (setf y num)
               )
      count))

  (print (part-1))
  (print (part-2)))


(defun day-2 ()
  "The second day"
  (defun part-1 ()
    "Interpret the steering instructions for the submarine"
    (let* ((directions (parse-file-into-list (abspath "../inputs/day-2.txt")))
           (horiz 0)
           (depth 0)
           (direction "")
           (amount 0)
           (step))
      (iterate (for d in directions)
               (setf step (split-str d))
               (setf direction (first step))
               (setf amount (parse-integer (second step)))

               (if (string= direction "up")
                   (setf depth (- depth amount)))
               (if (string= direction "down")
                   (setf depth (+ depth amount)))
               (if (string= direction "forward")
                   (setf horiz (+ horiz amount)))

               (print (list horiz depth))
               (print (* horiz depth)))))

  (defun part-2 ()
    "Interpret steering instructions wrt aim"
    (let* ((directions (parse-file-into-list (abspath "../inputs/day-2.txt")))
           (horiz 0)
           (depth 0)
           (aim 0)
           (direction "")
           (amount 0)
           (step))
      (iterate (for d in directions)
               (setf step (split-str d))
               (setf direction (first step))
               (setf amount (parse-integer (second step)))

               (if (string= direction "up")
                   (setf aim (- aim amount)))
               (if (string= direction "down")
                   (setf aim (+ aim amount)))
               (if (string= direction "forward")
                   (progn
                     (setf horiz (+ horiz amount))
                     (setf depth (+ depth (* aim amount)))))

               (print (list horiz depth))
               (print (* horiz depth)))))
  (part-1)
  (part-2))


(defun day-3 ()
  "The third day"
  (defun place-counts ()
    "Check the power consumption (find the most-used bit in each position)"
    (let* ((diagnostics (parse-file-into-list (abspath "../inputs/day-3.txt")))
           (counts (make-list (length (first diagnostics)) :initial-element 0)))
      (iterate (for d in diagnostics)
               (iterate (for place index-of-string d)
                        (setf (nth place counts)
                              (+ (nth place counts)
                                 (digit-char-p (char d place))))))
      ;110000111111 - 3135
      ;001111000000 - 960
      (return-from place-counts counts)
      ))
  (defun part-2 ()
    "Verify the life support rating"

    (defun filter-diagnostics (place diagnostics total-length bit-value)
      "keep only diagnostics with in place"
      (if (= 0 (length diagnostics))
         (return-from filter-diagnostics diagnostics))

      (let* ((most-significant-bit (if (>= (nth place (place-counts))
                                      (/ total-length 2))
                                  "1" "0"))
             (least-significant-bit (if (<= (nth place (place-counts))
                                      (/ total-length 2))
                                  "1" "0")))
        (print least-significant-bit)
        (iterate (for d in diagnostics)
                 (if (string= bit-value "1")
                     (if (string= most-significant-bit (char d place))
                         (collect d))
                     (if (string= least-significant-bit (char d place))
                         (collect d)))
                 )))


    (let* ((diagnostics (parse-file-into-list (abspath "../inputs/day-3.txt")))
           (diagnostics-2 (copy-list diagnostics))
           (list-size (length diagnostics)))
      (iterate (for i from 0 below (length (first diagnostics)))
               (setf diagnostics (filter-diagnostics i diagnostics list-size "1"))
               (if (= 1 (length diagnostics))
                   (finish)))
      (print diagnostics)

      (iterate (for i from 0  to 7);below (length (first diagnostics-2)))
               (setf diagnostics-2 (filter-diagnostics i diagnostics-2 list-size "0"))
               (if (= 1 (length diagnostics-2))
                   (finish)))
      (print diagnostics-2)
      )
    )
  (print (place-counts))
  ; 110000111011 - 3131
  ; 001111001001 - 969

  (part-2))


(defun day-4 ()
  "The fourth day"
  (defclass/std board ()
    ((cells :std (list 0 0 0 0 ))
     (matches :std (list))))
  (defmethod print-board (board)
    (print (slot-value board 'cells)))
  (defmethod print-matches (board)
    (print (slot-value board 'matches)))
  (defmethod draw-number (board number)
    "Given a board and a number, mark the number if it exists on the board"
    (let ((row-num 0)
          (match nil))
      (iterate (for row in (slot-value board 'cells))
               (setf match (position number row :test #'string=))
               (if match
                   (setf (matches board)
                         (append (matches board) (list (list row-num match number)))))
               (setf row-num (+ 1 row-num))
               (setf match nil))))
  (defmethod winning-p (board)
    "Does the board have a winning row?"
    (let ((x (map 'list #'first (slot-value board 'matches)))
          (y (map 'list #'second (slot-value board 'matches))))
      (iterate (for i from 0 to 4)
               (if (>= 5 (funcall #'count i x))
                   (return-from winning-p T)
                   )
               (if (>= 5 (funcall #'count i y))
                   (return-from winning-p T)
                   )

               )))


  (defun build-board (rows)
    "Build a bingo board from supplied list of space-separated rows"
    (make-instance 'board :cells
      (iterate (for row in rows)
               (collect (remove-if #'empty-string-p
                                   (split-str (string-trim " " row)))))))

  (defun part-1 ()
    (let* ((input (parse-file-into-list-by-blank-line (abspath "../inputs/day-4.txt")))
           (numbers (split-str (first (car input)) ","))
           (boards (cdr input))
           (test-board (build-board (first boards)))
           )
      (iterate (for draw in (subseq numbers 0 45))
               (draw-number test-board draw)
               (if (winning-p test-board)
                   (progn
                     (print (format nil "won on: ~S" draw))
                     (finish)
                     )
                   )
               )
      (print-matches test-board)
      (print-board test-board)
      (winning-p test-board)
      )
    )
  (defun part-2 ()
    )
  (part-1)
  (part-2))


(defun template ()
  "The  day"
  (defun part-1 ()
    )
  (defun part-2 ()
    )
  (part-1)
  (part-2))
