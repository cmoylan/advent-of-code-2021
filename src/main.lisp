(in-package :cl-user)
(defpackage aoc2021
  (:use :cl)
  (:import-from :aoc2021/days :day-1 :day-2 :day-3 :day-4)
  (:export :run))
(in-package :aoc2021)


(defun run()
  "Do it"
  ;(day-1)
  ;(day-2)
  ;(day-3)
  (day-4)
  )
