(in-package :cl-user)
(defpackage tools
  (:use :cl)
  (:export :empty-string-p
           :parse-file-into-list
           :abspath
           :parse-file-into-list-by-blank-line
           :split-str)
  )
(in-package :tools)


(defun empty-string-p (string)
  "Return true if the STRING is empty or nil. Expects string type."
  (or (null string)
      (zerop (length string))))


(defun parse-file-into-list (filename)
  "Parse a file into a list, splitting by newlines"
  (let ((input-list (list)))
    (with-open-file (stream filename)
      (do ((line (read-line stream nil)
                 (read-line stream nil)))
          ((null line))
        (unless (empty-string-p line)
          (setf input-list
                (append input-list
                        (list line))))))
    (return-from parse-file-into-list input-list)))


(defun parse-file-into-list-by-blank-line (filename)
  "Parse a file into a list, splitting on blank lines"
  (let ((group (list))
        (result (list)))
    (with-open-file (stream filename)
      (do ((line (read-line stream nil)
                 (read-line stream nil)))
          ((null line))
        (if (empty-string-p line)
            (progn
              (push group result)
              (setf group (list)))
            (push line group))))
    (push group result) ; do the last one
    (return-from parse-file-into-list-by-blank-line (reverse result))))


(defun parse-file-into-number-list (filename)
  (map 'list #'parse-integer
       (parse-file-into-list filename)))


(defun abspath (pathname)
  (merge-pathnames pathname *default-pathname-defaults*))


(defun count-matches (regex string)
  "Return the number of matches"
  (let ((match-index-count (length (ppcre:all-matches regex string))))
    (if (< 0 match-index-count)
        (/ 2 match-index-count)
        0)))


(defun split-str (string &optional (separator " "))
  (split-1 string separator))

(defun split-1 (string &optional (separator " ") (r nil))
  (let ((n (position separator string
		     :from-end t
		     :test #'(lambda (x y)
			       (find y x :test #'string=)))))
    (if n
	(split-1 (subseq string 0 n) separator (cons (subseq string (1+ n)) r))
      (cons string r))))
