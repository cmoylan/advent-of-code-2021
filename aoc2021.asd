(defsystem "aoc2021"
  :version "1.0.0"
  :author "Chris Moylan"
  :description "Advent of Code 2021"
  :depends-on (:iterate :cl-ppcre :str :alexandria :defclass-std)
  :components ((:module "src"
                :components
                ((:file "main" :depends-on ("days"))
                 (:file "tools")
                 (:file "days" :depends-on ("tools"))))
               )
  )
